namespace Bll.Migrations
{
    using DB;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Bll.DB.LibrexandraDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            SetSqlGenerator("MySql.Data.MySqlClient", new MySql.Data.Entity.MySqlMigrationSqlGenerator());
        }

        protected override void Seed(Bll.DB.LibrexandraDbContext context)
        {
            var adminRole = new IdentityRole { Name = "Admin" };

            var adminUser = new User
            {
                UserName = "admin",
                Email = "librexandra@sendow.me",
            };

            if (!context.Roles.Any(r => r.Name == adminRole.Name))
            {
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                roleManager.Create(adminRole);
            }

            if (!context.Users.Any(u => u.UserName == adminUser.UserName))
            {
                var userManager = new UserManager<User>(new UserStore<User>(context));
                var result = userManager.Create(adminUser, "admin1");
                userManager.AddToRole(adminUser.Id, adminRole.Name);
            }

            context.SaveChanges();
        }
    }
}
