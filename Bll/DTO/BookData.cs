﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.DTO
{
    public class BookData : BookHeaderData
    {
        [Display(Name = "Book_Description", ResourceType = typeof(Resources.LocalizedText))]
        public string Description { get; set; }
    }
}
