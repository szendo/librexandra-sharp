﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.DTO
{
    public class CommentData
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Text { get; set; }
        public string UserName { get; set; }
        public CommentStatus Status { get; set; }
    }
}
