﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.DTO
{
    public class CommentExtraData : CommentData
    {
        public int BookId { get; set; }
        public string BookAuthor { get; set; }
        public string BookTitle { get; set; }
        public int BookYear { get; set; }
    }
}
