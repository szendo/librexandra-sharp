﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.DTO
{
    public class BookListData : BookListHeaderData
    {
        public string UserId { get; set; }
        public List<BookHeaderData> Entires { get; set; }
    }
}
