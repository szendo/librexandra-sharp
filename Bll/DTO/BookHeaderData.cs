﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.DTO
{
    public class BookHeaderData
    {
        public int Id { get; set; }

        [Display(Name = "Book_Author", ResourceType = typeof(Resources.LocalizedText))]
        public string Author { get; set; }

        [Display(Name = "Book_Title", ResourceType = typeof(Resources.LocalizedText))]
        public string Title { get; set; }

        [Display(Name = "Book_Year", ResourceType = typeof(Resources.LocalizedText))]
        public int Year { get; set; }
    }
}
