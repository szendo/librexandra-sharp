﻿using Bll.DB;
using Bll.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll
{
    public class BookListManager
    {
        public List<BookListHeaderData> GetBookListsForUser(string userId)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.BookLists
                    .Where(l => l.UserId == userId)
                    .OrderBy(l => l.Timestamp)
                    .Select(l => new BookListHeaderData
                    {
                        Id = l.Id,
                        Name = l.Name
                    })
                    .ToList();
            }
        }

        public List<BookListHeaderData> GetLatestBookListsForBook(int bookId)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.BookListEntries
                    .Where(e => e.BookId == bookId)
                    .Select(e => e.BookList)
                    .OrderByDescending(l => l.Timestamp)
                    .Select(l => new BookListHeaderData
                    {
                        Id = l.Id,
                        Name = l.Name
                    })
                    .Take(5)
                    .ToList();
            }
        }

        public BookListData GetBookListById(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var bookList = context.BookLists.Find(id);
                return new BookListData
                {
                    Id = bookList.Id,
                    Name = bookList.Name,
                    UserId = bookList.UserId,
                    Entires = bookList.BookListEntires
                        .Select(e => e.Book)
                        .Select(b => new BookHeaderData
                        {
                            Id = b.Id,
                            Author = b.Author,
                            Title = b.Title,
                            Year = b.Year
                        }).ToList()
                };
            }
        }

        public void Create(BookList bookList)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                context.BookLists.Add(bookList);
                context.SaveChanges();
            }
        }

        public bool DeleteBookListForUser(int id, string userId)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var bookList = context.BookLists.Find(id);

                if (bookList?.UserId != userId)
                {
                    return false;
                }

                context.BookLists.Remove(bookList);
                context.SaveChanges();
                return true;
            }
        }

        public bool AddBookToList(int id, int bookId, string userId)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var bookList = context.BookLists.Find(id);

                if (bookList?.UserId != userId)
                {
                    return false;
                }

                var entry = new BookListEntry
                {
                    BookListId = id,
                    BookId = bookId,
                    Timestamp = DateTime.Now
                };

                bookList.BookListEntires.Add(entry);

                context.SaveChanges();
                return true;
            }
        }

        public bool RemoveBookFromList(int id, int bookId, string userId)
        {
            using (var context = LibrexandraDbContext.Create())
            {

                var entry = context.BookListEntries
                    .Where(e => e.BookListId == id && e.BookId == bookId)
                    .FirstOrDefault();

                if (entry?.BookList?.UserId != userId)
                {
                    return false;
                }

                context.BookListEntries.Remove(entry);

                context.SaveChanges();
                return true;
            }
        }
    }
}
