﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.DB
{
    [Table("Rating")]
    public partial class Rating
    {
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public int BookId { get; set; }

        [Required]
        [Range(0, 10)]
        public int Value { get; set; }

        public virtual User User { get; set; }

        public virtual Book Book { get; set; }

    }
}
