﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.DB
{
    [Table("Book")]
    public partial class Book
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Author { get; set; }

        [Required]
        [MaxLength(200)]
        public string Title { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        public byte[] Cover { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public BookStatus Status { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Rating> Ratings { get; set; }

        public virtual ICollection<BookListEntry> BookListEntires { get; set; }

    }
}
