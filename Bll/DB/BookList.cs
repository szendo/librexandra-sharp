﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.DB
{
    [Table("BookList")]
    public partial class BookList
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public DateTime Timestamp { get; set; }
        public string Name { get; set; }

        public virtual ICollection<BookListEntry> BookListEntires { get; set; }
    }
}
