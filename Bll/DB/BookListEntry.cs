﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.DB
{
    [Table("BookListEntry")]
    public partial class BookListEntry
    {
        public int Id { get; set; }
        public int BookListId { get; set; }
        public int BookId { get; set; }
        public DateTime Timestamp { get; set; }

        virtual public BookList BookList { get; set; }
        virtual public Book Book { get; set; }
    }
}
