﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.DB
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public partial class LibrexandraDbContext : IdentityDbContext<User>
    {
        public LibrexandraDbContext() : base("name=DefaultConnection")
        {
            //Database.SetInitializer(new LibrexandraDbInitializer());
        }

        static LibrexandraDbContext()
        {
            DbConfiguration.SetConfiguration(new MySql.Data.Entity.MySqlEFConfiguration());
        }

        public static LibrexandraDbContext Create()
        {
            return new LibrexandraDbContext();
        }

        virtual public DbSet<Book> Books { get; set; }
        virtual public DbSet<Comment> Comments { get; set; }
        virtual public DbSet<Rating> Ratings { get; set; }
        virtual public DbSet<BookList> BookLists { get; set; }
        virtual public DbSet<BookListEntry> BookListEntries { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityRole>()
                .Property(c => c.Name)
                .HasMaxLength(128)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.UserName)
                .HasMaxLength(128)
                .IsRequired();

            modelBuilder.Entity<Book>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.Book)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Book>()
                .HasMany(e => e.Ratings)
                .WithRequired(e => e.Book)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Book>()
                .HasMany(e => e.BookListEntires)
                .WithRequired(e => e.Book)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BookList>()
                .HasMany(e => e.BookListEntires)
                .WithRequired(e => e.BookList)
                .WillCascadeOnDelete(true);
        }

    }
}
