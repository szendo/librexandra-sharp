﻿using Bll.DB;
using Bll.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll
{
    public class CommentManager
    {
        public List<CommentData> GetCommentsForBook(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Comments
                    .Where(c => c.BookId == id)
                    .OrderBy(c => c.Timestamp)
                    .Select(c => new CommentData
                    {
                        Id = c.Id,
                        Timestamp = c.Timestamp,
                        Text = c.Text,
                        UserName = c.User.UserName,
                        Status = c.Status
                    })
                    .ToList();
            }
        }

        public List<CommentExtraData> GetReportedComments()
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Comments
                    .Where(c => c.Status == CommentStatus.Reported)
                    .Join(context.Books, c => c.BookId, b => b.Id, (c, b) => new CommentExtraData
                    {
                        Id = c.Id,
                        Timestamp = c.Timestamp,
                        Text = c.Text,
                        UserName = c.User.UserName,
                        Status = c.Status,
                        BookId = c.BookId,
                        BookAuthor = b.Author,
                        BookTitle = b.Title,
                        BookYear = b.Year
                    })
                    .OrderBy(c => c.Timestamp)
                    .ToList();
            }
        }

        public int GetReportedCommentCount()
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Comments
                    .Where(c => c.Status == CommentStatus.Reported)
                    .Count();
            }
        }

        public CommentData GetCommentById(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var c = context.Comments.Find(id);
                return new CommentData
                {
                    Id = c.Id,
                    Timestamp = c.Timestamp,
                    Text = c.Text,
                    UserName = c.User.UserName,
                    Status = c.Status
                };
            }
        }

        public int? GetBookIdForComment(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var comment = context.Comments.Find(id);
                return comment?.BookId;
            }
        }

        public void Create(Comment comment)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                context.Comments.Add(comment);
                context.SaveChanges();
            }
        }

        public bool ReportCommentById(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var comment = context.Comments.Find(id);

                if (comment?.Status != CommentStatus.Original)
                {
                    return false;
                }

                comment.Status = CommentStatus.Reported;
                context.SaveChanges();
                return true;
            }
        }

        public bool VerifyCommentById(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var comment = context.Comments.Find(id);

                if (comment?.Status != CommentStatus.Reported)
                {
                    return false;
                }

                comment.Status = CommentStatus.Verified;
                context.SaveChanges();
                return true;
            }
        }

        public bool EditCommentById(int id, string newText)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var comment = context.Comments.Find(id);

                if (comment?.Status != CommentStatus.Reported)
                {
                    return false;
                }

                comment.Status = CommentStatus.Edited;
                comment.Text = newText;
                context.SaveChanges();
                return true;
            }
        }

        public bool DeleteCommentById(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var comment = context.Comments.Find(id);

                if (comment?.Status != CommentStatus.Reported)
                {
                    return false;
                }

                context.Comments.Remove(comment);
                context.SaveChanges();
                return true;
            }
        }
    }
}
