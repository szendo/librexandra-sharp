﻿using Bll.DB;
using Bll.DTO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll
{
    public class BookManager
    {
        public IPagedList<BookHeaderData> GetAcceptedBooks(int pageNumber, int pageSize)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Books
                    .Where(b => b.Status == BookStatus.Accepted)
                    .OrderBy(b => b.Title)
                    .ThenBy(b => b.Author)
                    .Select(b => new BookHeaderData
                    {
                        Id = b.Id,
                        Author = b.Author,
                        Title = b.Title,
                        Year = b.Year
                    })
                    .ToPagedList(pageNumber, pageSize);
            }
        }

        public List<BookData> GetAcceptedBooksByFilterForList(string query, int listId)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Books
                    .Where(b => b.Status == BookStatus.Accepted && (b.Title.Contains(query) || b.Author.Contains(query)))
                    //.Where(b => !b.BookListEntires.Select(e => e.BookListId).Contains(listId))
                    //.Where(b => !b.BookListEntires.Any(e => e.BookListId == listId))
                    .Where(b => b.BookListEntires.Where(e => e.BookListId == listId).Select(e => (int?)e.Id).FirstOrDefault() == null)
                    .OrderBy(b => b.Author)
                    .ThenBy(b => b.Title)
                    .Select(b => new BookData
                    {
                        Id = b.Id,
                        Author = b.Author,
                        Title = b.Title,
                        Year = b.Year,
                        Description = b.Description,
                    })
                    .ToList();
            }
        }

        public BookData GetAcceptedBookById(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Books
                    .Where(b => b.Id == id && b.Status == BookStatus.Accepted)
                    .Select(b => new BookData
                    {
                        Id = b.Id,
                        Author = b.Author,
                        Title = b.Title,
                        Year = b.Year,
                        Description = b.Description,
                    })
                    .FirstOrDefault();
            }
        }

        public byte[] GetAcceptedBookCoverById(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Books
                    .Where(b => b.Id == id && b.Status == BookStatus.Accepted)
                    .Select(b => b.Cover)
                    .FirstOrDefault();
            }
        }

        public List<BookData> GetPendingBooks()
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Books
                    .Where(b => b.Status == BookStatus.Pending)
                    .Select(b => new BookData
                    {
                        Id = b.Id,
                        Author = b.Author,
                        Title = b.Title,
                        Year = b.Year,
                        Description = b.Description,
                    })
                    .ToList();
            }
        }

        public int GetPendingBookCount()
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Books
                    .Where(b => b.Status == BookStatus.Pending)
                    .Count();
            }
        }

        public byte[] GetBookCoverById(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Books
                    .Where(b => b.Id == id)
                    .Select(b => b.Cover)
                    .FirstOrDefault();
            }
        }

        public void Create(Book book)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                context.Books.Add(book);
                context.SaveChanges();
            }
        }

        public void AcceptPending(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var book = context.Books.Find(id);
                if (book != null && book.Status == BookStatus.Pending)
                {
                    book.Status = BookStatus.Accepted;
                    context.SaveChanges();
                }
            }
        }

        public void DeletePending(int id)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var book = context.Books.Find(id);
                if (book != null && book.Status == BookStatus.Pending)
                {
                    context.Books.Remove(book);
                    context.SaveChanges();
                }
            }
        }

    }
}
