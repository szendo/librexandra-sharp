﻿using Bll.DB;
using Bll.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll
{
    public class RatingManager
    {
        public double GetAverageRatingForBook(int bookId)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                return context.Ratings
                    .Where(r => r.BookId == bookId)
                    .Average(r => (int?)r.Value) ?? 0.0;
            }
        }

        public int? GetRatingForBookByUser(int bookId, string userId)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                try
                {
                    return context.Ratings
                        .Where(r => r.BookId == bookId && r.UserId == userId)
                        .Select(r => r.Value)
                        .First();
                }
                catch (InvalidOperationException)
                {
                    return null;
                }
            }
        }

        public void CreateOrUpdateRatingForBook(int bookId, string userId, int value)
        {
            using (var context = LibrexandraDbContext.Create())
            {
                var rating = context.Ratings
                    .Where(r => r.BookId == bookId && r.UserId == userId)
                    .FirstOrDefault();

                if (rating == null)
                {
                    rating = new Rating
                    {
                        BookId = bookId,
                        UserId = userId,
                        Value = value
                    };
                    context.Ratings.Add(rating);
                }
                else
                {
                    rating.Value = value;
                }

                context.SaveChanges();
            }
        }

    }
}
