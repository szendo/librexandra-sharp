﻿using Bll.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class BookFilterViewModel
    {
        public int BookListId { get; set; }
        public List<BookData> FilteredBooks { get; set; }
    }
}