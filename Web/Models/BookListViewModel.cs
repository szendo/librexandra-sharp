﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class BookListViewModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}