﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class BookViewModel
    {
        [Required(ErrorMessageResourceName = "General_Error_Required", ErrorMessageResourceType = typeof(Resources.LocalizedText))]
        [MaxLength(200, ErrorMessageResourceName = "General_Error_MaxLength", ErrorMessageResourceType = typeof(Resources.LocalizedText))]
        [Display(Name = "Book_Title", ResourceType = typeof(Resources.LocalizedText))]
        public string Title { get; set; }

        [Required(ErrorMessageResourceName = "General_Error_Required", ErrorMessageResourceType = typeof(Resources.LocalizedText))]
        [MaxLength(200, ErrorMessageResourceName = "General_Error_MaxLength", ErrorMessageResourceType = typeof(Resources.LocalizedText))]
        [Display(Name = "Book_Author", ResourceType = typeof(Resources.LocalizedText))]
        public string Author { get; set; }

        [Required(ErrorMessageResourceName = "General_Error_Required", ErrorMessageResourceType = typeof(Resources.LocalizedText))]
        [Range(0, 9999, ErrorMessageResourceName = "General_Error_Range", ErrorMessageResourceType = typeof(Resources.LocalizedText))]
        [Display(Name = "Book_Year", ResourceType = typeof(Resources.LocalizedText))]
        public int Year { get; set; }

        [Required(ErrorMessageResourceName = "General_Error_Required", ErrorMessageResourceType = typeof(Resources.LocalizedText))]
        [DataType(DataType.MultilineText)]
        [MaxLength(2000, ErrorMessageResourceName = "General_Error_MaxLength", ErrorMessageResourceType = typeof(Resources.LocalizedText))]
        [Display(Name = "Book_Description", ResourceType = typeof(Resources.LocalizedText))]
        public string Description { get; set; }

        [Required(ErrorMessageResourceName = "General_Error_Required", ErrorMessageResourceType = typeof(Resources.LocalizedText))]
        [DataType(DataType.Upload)]
        [Display(Name = "Book_Cover", ResourceType = typeof(Resources.LocalizedText))]
        public HttpPostedFileBase CoverImage { get; set; }

    }
}