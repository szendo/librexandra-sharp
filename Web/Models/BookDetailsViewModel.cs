﻿using Bll.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Models
{
    public class BookDetailsViewModel
    {
        public BookData Book { get; set; }
        public List<CommentData> Comments { get; set; }
        public double AverageRating { get; set; }
        public int? UserRating { get; set; }
        public IEnumerable<SelectListItem> RatingSelectList { get; set; }
        public List<BookListHeaderData> RelatedLists { get; set; }
    }
}