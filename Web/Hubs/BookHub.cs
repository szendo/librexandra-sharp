﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

namespace Web.Hubs
{
    public class BookHub : Hub
    {
        public async Task JoinRoom(int bookId)
        {
            await Groups.Add(Context.ConnectionId, bookId.ToString());
        }

        public static void UpdateComments(int? bookId)
        {
            if (bookId != null)
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<BookHub>();
                hubContext.Clients.Group(bookId.ToString()).updateComments();
            }
        }

        public static void UpdateRating(int? bookId, double rating)
        {
            if (bookId != null)
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<BookHub>();
                hubContext.Clients.Group(bookId.ToString()).updateRating(rating);
            }
        }
    }
}