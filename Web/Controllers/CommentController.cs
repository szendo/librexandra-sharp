﻿using Bll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Hubs;

namespace Web.Controllers
{
    [Authorize]
    public class CommentController : Controller
    {
        // POST: Comment/Report/5
        public ActionResult Report(int id)
        {
            var bookId = new CommentManager().GetBookIdForComment(id);
            if (new CommentManager().ReportCommentById(id))
            {
                BookHub.UpdateComments(bookId);
            }
            return RedirectToAction("Details", "Book", new { id = bookId });
        }
    }
}