﻿using Bll;
using Bll.DB;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    [Authorize]
    public class ListController : Controller
    {
        // GET: List
        public ActionResult Index()
        {
            var model = new BookListManager().GetBookListsForUser(User.Identity.GetUserId());
            return View(model);
        }

        // GET: List/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id)
        {
            var model = new BookListManager().GetBookListById(id);
            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        // GET: List/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: List/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BookListViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var bookList = new BookList
                {
                    Name = model.Name,
                    Timestamp = DateTime.Now,
                    UserId = User.Identity.GetUserId()
                };
                new BookListManager().Create(bookList);

                return RedirectToAction("Edit", new { id = bookList.Id });
            }
            catch
            {
                return View(model);
            }
        }

        // GET: List/Edit/5
        public ActionResult Edit(int id)
        {
            var model = new BookListManager().GetBookListById(id);

            if (model.UserId != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
            }

            return View(model);
        }

        // POST: List/AddBook/5?bookId=3
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBook(int id, int bookId)
        {
            var result = new BookListManager().AddBookToList(id, bookId, User.Identity.GetUserId());

            if (result)
            {
                return RedirectToAction("Edit", new { id = id });
            }
            else
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
            }
        }

        // POST: List/RemoveBook/5?bookId=3
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveBook(int id, int bookId)
        {
            var result = new BookListManager().RemoveBookFromList(id, bookId, User.Identity.GetUserId());

            if (result)
            {
                return RedirectToAction("Edit", new { id = id });
            }
            else
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
            }
        }

        // GET: List/Delete/5
        public ActionResult Delete(int id)
        {
            var model = new BookListManager().GetBookListById(id);

            if (model.UserId != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
            }

            return View(model);
        }

        // POST: List/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                new BookListManager().DeleteBookListForUser(id, User.Identity.GetUserId());

                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }
    }
}
