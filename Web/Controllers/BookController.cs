﻿using Bll;
using Bll.DB;
using Bll.DTO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Helpers;
using Web.Hubs;

namespace Web.Controllers
{
    [Authorize]
    public class BookController : Controller
    {
        // GET: Book
        [AllowAnonymous]
        public ActionResult Index(int pageNumber = 1)
        {
            var model = new BookManager().GetAcceptedBooks(pageNumber, Config.BooksPageSize);
            return View(model);
        }

        // GET: Book/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id)
        {
            var model = new BookDetailsViewModel();
            model.Book = new BookManager().GetAcceptedBookById(id);

            if (model.Book != null)
            {
                var ratingManager = new RatingManager();
                model.AverageRating = ratingManager.GetAverageRatingForBook(bookId: id);
                if (User.Identity.IsAuthenticated)
                {
                    model.UserRating = ratingManager.GetRatingForBookByUser(bookId: id, userId: User.Identity.GetUserId());
                }

                model.RatingSelectList = Enumerable.Range(1, 10).Select(n => new SelectListItem
                {
                    Text = n.ToString(),
                    Value = n.ToString(),
                    Selected = n == model.UserRating
                });

                model.RelatedLists = new BookListManager().GetLatestBookListsForBook(id);
                model.Comments = new CommentManager().GetCommentsForBook(id);

                if (TempData.ContainsKey("PostCommentError"))
                {
                    ViewBag.PostCommentError = TempData["PostCommentError"];
                }

                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // GET: Book/CoverImage/5
        [AllowAnonymous]
        public ActionResult CoverImage(int id)
        {
            var bookManager = new BookManager();
            var model = User.IsInRole("Admin") ? bookManager.GetBookCoverById(id) : bookManager.GetAcceptedBookCoverById(id);
            if (model != null)
            {
                return new FileContentResult(model, "image/png");
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Book/ListComments/5
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ListComments(int id)
        {
            var model = new CommentManager().GetCommentsForBook(id);
            return PartialView(model);
        }

        // POST: Book/PostComment/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult PostComment(int id, FormCollection collection)
        {
            try
            {
                var comment = new Comment
                {
                    BookId = id,
                    UserId = User.Identity.GetUserId(),
                    Timestamp = DateTime.Now,
                    Text = collection["text"],
                    Status = CommentStatus.Original
                };
                new CommentManager().Create(comment);
                BookHub.UpdateComments(id);
            }
            catch
            {
                TempData["PostCommentError"] = "Posting comment failed!";
            }
            return RedirectToAction("Details", new { id = id });
        }

        // POST: Book/PostRating/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostRating(int id, FormCollection collection)
        {

            int rating;
            if (int.TryParse(collection["rating"], out rating))
            {
                var ratingManager = new RatingManager();
                ratingManager.CreateOrUpdateRatingForBook(id, User.Identity.GetUserId(), rating);

                var averageRating = ratingManager.GetAverageRatingForBook(id);
                BookHub.UpdateRating(id, averageRating);
            }

            return RedirectToAction("Details", new { id = id });
        }

        // GET: Book/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Book/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BookViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var resizedCover = ImageHelper.ResizeImage(model.CoverImage.InputStream, 360, 480);
                var book = new Book
                {
                    Author = model.Author,
                    Title = model.Title,
                    Year = model.Year,
                    Description = model.Description,
                    Cover = resizedCover.GetBuffer(),
                    Status = BookStatus.Pending
                };
                new BookManager().Create(book);

                return RedirectToAction("ThankYou");
            }
            catch (ImageProcessor.Common.Exceptions.ImageFormatException)
            {
                ModelState.AddModelError("CoverImage", "Invalid Cover uploaded");
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Book/ThankYou
        public ActionResult ThankYou()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FilterForList(string q, int listId)
        {
            var model = new BookFilterViewModel
            {
                BookListId = listId,
                FilteredBooks = new BookManager().GetAcceptedBooksByFilterForList(q, listId)
            };
            return PartialView(model);
        }
    }
}
