﻿using Bll;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Web.Hubs;
using Web.Models;

namespace Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            ViewBag.PendingBooks = new BookManager().GetPendingBookCount();
            ViewBag.PendingComments = new CommentManager().GetReportedCommentCount();
            return View();
        }

        // GET: Admin/ManageBooks
        public ActionResult ManageBooks()
        {
            var model = new BookManager().GetPendingBooks();
            return View(model);
        }

        // POST: Admin/ManageBook/5
        [HttpPost]
        public ActionResult ManageBook(int id, FormCollection collection)
        {
            var bookManager = new BookManager();
            if (collection["accept"] != null)
            {
                bookManager.AcceptPending(id);
            }
            else if (collection["delete"] != null)
            {
                bookManager.DeletePending(id);
            }
            return RedirectToAction("ManageBooks");
        }

        // GET: Admin/ManageUsers
        public async Task<ActionResult> ManageUsers()
        {
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var model = userManager.Users
                .Select(u => new UserViewModel
                {
                    Id = u.Id,
                    Username = u.UserName,
                }).ToList();

            foreach (var user in model)
            {
                user.IsAdmin = await userManager.IsInRoleAsync(user.Id, "Admin");
                user.IsBanned = await userManager.IsLockedOutAsync(user.Id);
            }

            return View(model);
        }

        // POST: Admin/ManageUser/ed7ba470-8e54-465e-825c-99712043e01c
        [HttpPost]
        public ActionResult ManageUser(string id, FormCollection collection)
        {
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (collection["promote"] != null)
            {
                userManager.AddToRole(id, "Admin");
            }
            else if (collection["demote"] != null)
            {
                userManager.RemoveFromRole(id, "Admin");
            }
            else if (collection["lockout"] != null)
            {
                userManager.RemoveFromRole(id, "Admin");
                userManager.SetLockoutEnabled(id, true);
                userManager.SetLockoutEndDate(id, DateTimeOffset.Now.AddYears(999));
            }
            else if (collection["pardon"] != null)
            {
                userManager.SetLockoutEndDate(id, DateTimeOffset.Now);
            }
            return RedirectToAction("ManageUsers");
        }

        // GET: Admin/ManageComments
        public ActionResult ManageComments()
        {
            var model = new CommentManager().GetReportedComments();
            return View(model);
        }

        // GET: Admin/EditComment/5
        public ActionResult EditComment(int id)
        {
            var model = new CommentManager().GetCommentById(id);
            return View(model);
        }

        // POST: Admin/VerifyComment/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VerifyComment(int id, FormCollection collection)
        {
            var bookId = new CommentManager().GetBookIdForComment(id);
            if (new CommentManager().VerifyCommentById(id))
            {
                BookHub.UpdateComments(bookId);
            }
            return RedirectToAction("ManageComments");
        }

        // POST: Admin/EditComment/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditComment(int id, FormCollection collection)
        {

            var bookId = new CommentManager().GetBookIdForComment(id);
            if (new CommentManager().EditCommentById(id, collection["text"]))
            {
                BookHub.UpdateComments(bookId);
            }
            return RedirectToAction("ManageComments");
        }

        // POST: Admin/DeleteComment/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteComment(int id, FormCollection collection)
        {
            var bookId = new CommentManager().GetBookIdForComment(id);
            if (new CommentManager().DeleteCommentById(id))
            {
                BookHub.UpdateComments(bookId);
            }
            return RedirectToAction("ManageComments");
        }
    }
}