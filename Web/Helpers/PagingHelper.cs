﻿using PagedList.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Helpers.Mvc
{
    public class PagingHelper
    {
        public static PagedListRenderOptions CustomPagedListRenderOptions
        {
            get
            {
                return new PagedListRenderOptions
                {
                    LinkToFirstPageFormat = "<span class=\"fa fa-angle-double-left\"></span>",
                    LinkToPreviousPageFormat = "<span class=\"fa fa-angle-left\"></span>",
                    LinkToNextPageFormat = "<span class=\"fa fa-angle-right\"></span>",
                    LinkToLastPageFormat = "<span class=\"fa fa-angle-double-right\"></span>",
                };
            }
        }
    }
}