﻿using ImageProcessor;
using ImageProcessor.Imaging;
using ImageProcessor.Imaging.Formats;
using System.Drawing;
using System.IO;

namespace Web.Helpers
{
    public static class ImageHelper
    {
        public static MemoryStream ResizeImage(Stream original, int width, int height)
        {
            using (var resized = new MemoryStream())
            {
                using (ImageFactory imageFactory = new ImageFactory())
                {
                    imageFactory.Load(original)
                                .Resize(new ResizeLayer(new Size(width, height), ResizeMode.Max, upscale: false))
                                .Format(new PngFormat())
                                .Save(resized);
                }
                return resized;
            }
        }
    }
}