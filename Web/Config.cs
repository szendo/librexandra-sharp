﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Web
{
    public class Config
    {
        public static int BooksPageSize
        {
            get
            {
                return int.Parse(WebConfigurationManager.AppSettings["BooksPageSize"]);
            }
        }

        public static string GoogleAuthClientId
        {
            get
            {
                return WebConfigurationManager.AppSettings["GoogleAuthClientId"];
            }
        }

        public static string GoogleAuthClientSecret
        {
            get
            {
                return WebConfigurationManager.AppSettings["GoogleAuthClientSecret"];
            }
        }
    }
}